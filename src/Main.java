public class Main {

    //static Variables
    static boolean rerun = true, exceptionThrown = false;

    public static void main(String[] args) {

        double x, y, result; // x and y represent the two operators
        char op;
        boolean rerun = true, exceptionThrown = false;
        String[] strInputArray;

        // Initialize Objects
        Display display = new Display();
        IOControl io = new IOControl();
        Mapper map = new Mapper();
        Operations operations = new Operations();

        // Intro
        display.introduction();

        while (rerun && !exceptionThrown) {
            // Basic Calculator
            strInputArray = io.takeInput();
            x = map.stringToVal(strInputArray[0]);
            y = map.stringToVal(strInputArray[1]);
            op = strInputArray[2].charAt(0);
            result = operations.calculate(x, y, op);

            // Print Outro only if exception is not thrown
            if (!exceptionThrown) {
                System.out.println("The calculated value is " + result);
                display.thanks();
            }

            // Prompt to rerun calc
            rerun = io.rerun();

        }

    }
}