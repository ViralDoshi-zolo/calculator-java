import java.util.Scanner;

public class IOControl {


    String[] takeInput(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter operands and operator.\t Format: <operand1> <operand2> <operator>");
        String strInput = sc.nextLine();
        String[] inpArray = strInput.split(" ");
        return inpArray;
    }

    boolean rerun(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you wish to continue ? \nEnter y for Yes and n for No");
        String choice = sc.nextLine();
        if (choice.toLowerCase().charAt(0) == 'y') {
            System.out.println("You have chosen to run the calc again");
            return true;
        }
        else if (choice.toLowerCase().charAt(0) == 'n') {
            System.out.println("You have chosen to exit");
            return false;
        }
        else {
            System.out.println("Invalid Input");
            return false;
        }
    }

}
