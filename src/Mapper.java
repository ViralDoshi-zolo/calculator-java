import java.text.ParseException;

public class Mapper {

    double stringToVal(String s){
        Double d;
        try {
            d = Double.parseDouble(s);
        }
        catch (NumberFormatException e){
            System.out.println("Operators x and y should be a decimal number only");
//            isCodeRunnable = false;
            return 0f;
        }
        return d;
    }
}
