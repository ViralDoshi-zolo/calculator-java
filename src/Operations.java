import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Operations {
    Map<Character, Method> calcMap = new HashMap<Character, Method>();
    static double result = 1d, val1, val2;

    public static void Addition(){ result = val1 + val2; }
    public static void Subtraction(){ result = val1 - val2; }
    public static void Multiplication(){ result = val1 * val2; }
    public static void Division(){ result = val1 / val2; }

    private void initializeMap(){
        try {
            calcMap.put('+', Operations.class.getMethod("Addition"));
            calcMap.put('-', Operations.class.getMethod("Subtraction"));
            calcMap.put('*', Operations.class.getMethod("Multiplication"));
            calcMap.put('/', Operations.class.getMethod("Division"));
        } catch (NoSuchMethodException e) {
            Main.exceptionThrown = true;
            System.out.println("Internal Code Error");
        }

    }

    public double calculate(double x, double y, char op){
        val1 = x;
        val2 = y;
        try{
            initializeMap();
            calcMap.get(op).invoke(null);
        }
        catch(Exception e){
            Main.exceptionThrown = true;
            System.out.println("Exception in calculate: " + e);
        }

        return result;
    }
}
